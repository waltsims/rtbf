#ifndef RTBF_TYPE_NAMES_MAT_CUH_
#define RTBF_TYPE_NAMES_MAT_CUH_
#include <complex>
#include <unordered_map>

#include "mex.hpp"

// Define a map from each MATLAB ArrayType to a string
std::unordered_map<matlab::data::ArrayType, std::string> matlab_type_names = {
    {matlab::data::ArrayType::LOGICAL, "LOGICAL"},
    {matlab::data::ArrayType::CHAR, "CHAR"},
    {matlab::data::ArrayType::MATLAB_STRING, "MATLAB_STRING"},
    {matlab::data::ArrayType::DOUBLE, "DOUBLE"},
    {matlab::data::ArrayType::SINGLE, "SINGLE"},
    {matlab::data::ArrayType::INT8, "INT8"},
    {matlab::data::ArrayType::UINT8, "UINT8"},
    {matlab::data::ArrayType::INT16, "INT16"},
    {matlab::data::ArrayType::UINT16, "UINT16"},
    {matlab::data::ArrayType::INT32, "INT32"},
    {matlab::data::ArrayType::UINT32, "UINT32"},
    {matlab::data::ArrayType::INT64, "INT64"},
    {matlab::data::ArrayType::UINT64, "UINT64"},
    {matlab::data::ArrayType::COMPLEX_DOUBLE, "COMPLEX_DOUBLE"},
    {matlab::data::ArrayType::COMPLEX_SINGLE, "COMPLEX_SINGLE"},
    {matlab::data::ArrayType::COMPLEX_INT8, "COMPLEX_INT8"},
    {matlab::data::ArrayType::COMPLEX_UINT8, "COMPLEX_UINT8"},
    {matlab::data::ArrayType::COMPLEX_INT16, "COMPLEX_INT16"},
    {matlab::data::ArrayType::COMPLEX_UINT16, "COMPLEX_UINT16"},
    {matlab::data::ArrayType::COMPLEX_INT32, "COMPLEX_INT32"},
    {matlab::data::ArrayType::COMPLEX_UINT32, "COMPLEX_UINT32"},
    {matlab::data::ArrayType::COMPLEX_INT64, "COMPLEX_INT64"},
    {matlab::data::ArrayType::COMPLEX_UINT64, "COMPLEX_UINT64"},
    {matlab::data::ArrayType::CELL, "CELL"},
    {matlab::data::ArrayType::STRUCT, "STRUCT"},
    {matlab::data::ArrayType::OBJECT, "OBJECT"},
    {matlab::data::ArrayType::VALUE_OBJECT, "VALUE_OBJECT"},
    {matlab::data::ArrayType::HANDLE_OBJECT_REF, "HANDLE_OBJECT_REF"},
    {matlab::data::ArrayType::ENUM, "ENUM"},
    {matlab::data::ArrayType::SPARSE_LOGICAL, "SPARSE_LOGICAL"},
    {matlab::data::ArrayType::SPARSE_DOUBLE, "SPARSE_DOUBLE"},
    {matlab::data::ArrayType::SPARSE_COMPLEX_DOUBLE, "SPARSE_COMPLEX_DOUBLE"},
    {matlab::data::ArrayType::UNKNOWN, "UNKNOWN"}};

std::string type_name(matlab::data::ArrayType t) {
  return matlab_type_names[t];
}

#endif