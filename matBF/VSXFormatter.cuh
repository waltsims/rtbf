/**
 @file vsxBF/utils/VSXFormatter.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-27

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef VSXDATAFORMATTER_CUH_
#define VSXDATAFORMATTER_CUH_

#include "Operator.cuh"

namespace rtbf {

/// @brief Enum class to describe VSX sampling mode
enum class VSXSampleMode { BS50BW, BS100BW, NS200BW, HILBERT };

/** @brief Enum class to describe data ordering

When performing Doppler acquisitions, five dimensions of data are acquired:
- Samples (i.e., fast time)
- Pulses (e.g., pulse-inversion harmonic imaging, averaging to improve SNR)
- Transmits (e.g., focused transmits or plane waves)
- Doppler Frames (i.e., the ensemble dimension)
- Channels

The sample dimension is always the fastest changing and the channel dimension is
always the slowest changing. Pulses, transmits, and Doppler frames can be
permuted arbitrarily depending on the application. DopplerFormat enumerates the
various permutations.

The following formats are supported:
  1. PXF = [Pulses, Transmits, Frames]  e.g., "ultrafast" Doppler
  2. PFX = [Pulses, Frames, Transmits]  e.g., traditional Doppler
  3. XPF = [Transmits, Pulses, Frames]
  4. FPX = [Frames, Pulses, Transmits]
  5. XFP = [Transmits, Frames, Pulses]
  6. FXP = [Frames, Transmits, Pulses]
*/
enum class VSXDataOrder { PXF, PFX, XPF, FPX, XFP, FXP };

/** @brief Class to format Verasonics channel data

This class accepts a host pointer to the raw receive channel buffer, copies it
over to a device pointer, and applies preprocessing.

There are three types of preprocessing that can be applied:
  1. Direct baseband sampling via Verasonics's sampleMode parameter
  2. Coherent summing of pulses (e.g., pulse-inversion harmonic imaging)
  3. Channel mapping unwrapping

The Verasonics scanner provides the option to directly sample the IQ baseband
data, via setting the "Receive.sampleMode" parameter. (See the Verasonics
Sequence Programming Manual for more details.) These modes can be selected by
setting the VSXSampleMode. Supported values are: NS200BW, BS100BW, and BS50BW.
Alternatively, the user can choose to treat the signals as the real component of
the RF signal and obtain the modulated IQ signal via the Hilbert transform using
the value HILBERT.

The class also provides the option to sum (i.e. coherently compound) multiple
input pulses. This is useful for cases like pulse-inversion harmonic imaging,
where the positive and negative phase pulses can be summed immediately to reduce
the data size.

Often, the channel mapping is some permutation of the element positions. When an
optional host pointer to the channel mapping is specified, the channels are
unwrapped automatically. Furthermore, the number of channels per acquisition may
differ from the total number of channels to be stored for processing (e.g.,
walked aperture, multiplexed transducers). In these cases, the user should
provide a mapping of acquired channels to actual channels on a per-transmit
basis via the h_channelMapping argument.

These operations are all fused into a single kernel to minimize the overhead
associated with launching a CUDA kernel.
*/

template <typename T_in, typename T_out>
class VSXFormatter : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  // VSXFormatter members
  int gpuID;       ///< Device to use for execution
  size_t nsamps;   ///< Number of IQ samples per transmit/receive event
  size_t nxmits;   ///< Number of transmit/receive events to store
  size_t nchans;   ///< Number of channels acquired at a time
  size_t nelems;   ///< Number of elements (must be >= nchans)
  size_t npreps;   ///< Number of repeated pulses to sum (e.g., pulse-inversion)
  size_t nframes;  ///< Number of frames to process (e.g., Doppler ensemble)
  int dsfactor;    ///< Decimation factor
  VSXSampleMode mode;                    ///< Data sample mode (e.g., NS200BW)
  std::shared_ptr<Tensor<int>> chanmap;  ///< Channel->element map per transmit
  std::shared_ptr<Tensor<cuda::std::complex<float>>>
      tmp;              ///< Temp. FFT array (complex)
  int pstride;          ///< Stride of the pulses
  int xstride;          ///< Stride of the transmits
  int fstride;          ///< Stride of the frames
  cufftHandle fftplan;  ///< CUFFT plan object
  int oframesize;       ///< Number of values in an output frame

  // Internal functions
  void unwrapData(short *idata);
  void filterData();
  void downsampleData(T_out *odata);

 public:
  VSXFormatter(
      std::shared_ptr<Tensor<T_in>> input, int numSamples, int numTransmits,
      int numChannels, int numAcqChannels,
      VSXSampleMode sampleMode = VSXSampleMode::NS200BW,
      std::optional<int> decimationFactor = std::nullopt,
      int numPulsesToSum = 1, int numFrames = 1,
      const std::optional<TensorView<int>> &h_channelMapping = std::nullopt,
      int deviceID = 0, cudaStream_t cudaStream = 0,
      std ::string moniker = "VSXFormatter", std::string loggerName = "");
  virtual ~VSXFormatter();

  /// @brief Free all dynamically allocated memory.
  void reset();

  /// @brief Function to execute formatting.
  void formatRawVSXData();
  /// @brief Set the order of the data (untested as of now)
  void setVSXDataOrder(VSXDataOrder order);
  /// @brief Retrieve the downsampling factor
  int getDSFactor() { return dsfactor; }
};

namespace kernels::VSXFormatter {
// CUDA kernels
// Unwrap the IQ into our fully-sampled temporary array with inserted zeros
template <int spc>
__global__ void unwrapIQ(int nsamps, int npulses, int nxmits, int nchans,
                         int nelems, int pstride, int xstride,
                         cuda::std::complex<short> *idata, int ipitch,
                         cuda::std::complex<float> *tdata, int tpitch,
                         int *cmap);
/** @brief Bandpass filtering for anti-aliasing the upsampled data.
Verasonics data is already demodulated. The upsampling procedure in
unwrapData results in an aliased spectrum. This kernel applies an
anti-aliasing bandpass filter whose band depends on the samples per cycle of
the VSXSampleMode.
*/
template <int spc>
__global__ void bandpassFilter(int nsamps, int nxmits, int nelems,
                               cuda::std::complex<float> *tdata, int tpitch);
/** @brief The final data is highly oversampled. Now that an anti-aliasing
 * bandpass filter has been applied, we can safely downsample the data without
 * loss of information.
 */
template <typename T_out>
__global__ void downsampleIQ(int nsamps_out, int nxmits, int nelems,
                             cuda::std::complex<float> *tdata, int tpitch,
                             T_out *out, int opitch, int dsf);
}  // namespace kernels::VSXFormatter

// Add template type traits
template <typename>
struct is_VSXFormatter : std::false_type {};
template <typename T_in, typename T_out>
struct is_VSXFormatter<VSXFormatter<T_in, T_out>> : std::true_type {};
}  // namespace rtbf

#endif
