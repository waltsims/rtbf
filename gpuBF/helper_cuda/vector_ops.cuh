/**
 @file gpuBF/helper_cuda/vector_ops.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2021-05-07

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef VECTOR_OPS_CUH_
#define VECTOR_OPS_CUH_

const __device__ float PI = 3.14159265f;

// ADDITION OPERATORS
// Scalar += operator overload for vector types with casting
inline __host__ __device__ void operator+=(float2 &a, int s) {
  a.x += (float)s;
  a.y += (float)s;
}
inline __host__ __device__ void operator+=(float2 &a, short s) {
  a.x += (float)s;
  a.y += (float)s;
}
inline __host__ __device__ void operator+=(int2 &a, short s) {
  a.x += (int)s;
  a.y += (int)s;
}

// Vector += operator overload for vector types with casting
inline __host__ __device__ void operator+=(float2 &a, int2 s) {
  a.x += (float)s.x;
  a.y += (float)s.y;
}
inline __host__ __device__ void operator+=(float2 &a, short2 s) {
  a.x += (float)s.x;
  a.y += (float)s.y;
}
inline __host__ __device__ void operator+=(int2 &a, short2 s) {
  a.x += (int)s.x;
  a.y += (int)s.y;
}
inline __host__ __device__ void operator+=(short2 &a, short2 s) {
  a.x += s.x;
  a.y += s.y;
}

// Scalar *= operator overload for vector types with casting
inline __host__ __device__ void operator*=(short2 &a, float s) {
  a.x = short(float(a.x) * s);
  a.y = short(float(a.y) * s);
}

// // Scalar *= operator overload for vector types
// inline __host__ __device__ void operator*=(float2 &a, float s) {
//   a.x *= s;
//   a.y *= s;
// }
// inline __host__ __device__ void operator*=(float2 &a, int s) {
//   a.x *= (float)s;
//   a.y *= (float)s;
// }
// inline __host__ __device__ void operator*=(float2 &a, short s) {
//   a.x *= (float)s;
//   a.y *= (float)s;
// }
// inline __host__ __device__ void operator*=(int2 &a, int s) {
//   a.x *= s;
//   a.y *= s;
// }
// inline __host__ __device__ void operator*=(int2 &a, short s) {
//   a.x *= (int)s;
//   a.y *= (int)s;
// }
// inline __host__ __device__ void operator*=(short2 &a, short s) {
//   a.x *= s;
//   a.y *= s;
// }

// // Scalar * operator overload for vector types
// inline __host__ __device__ float2 operator*(float2 a, float s) {
//   return make_float2(a.x * s, a.y * s);
// }
// inline __host__ __device__ float2 operator*(float s, float2 a) { return a *
// s; } inline __host__ __device__ float2 operator*(float2 a, int s) {
//   return make_float2(a.x * s, a.y * s);
// }
// inline __host__ __device__ float2 operator*(int s, float2 a) {
//   return make_float2(a.x * s, a.y * s);
// }

// inline __host__ __device__ void operator*=(float2 &a, int s) {
//   a.x *= (float)s;
//   a.y *= (float)s;
// }
// inline __host__ __device__ float2 operator*(float2 a, int s) {
//   return make_float2(a.x * (float)s, a.y * (float)s);
// }
// inline __host__ __device__ void operator*=(float2 &a, short s) {
//   a.x *= (float)s;
//   a.y *= (float)s;
// }
// inline __host__ __device__ float2 operator*(float2 a, short s) {
//   return make_float2(a.x * (float)s, a.y * (float)s);
// }

// inline __host__ __device__ void operator+=(int2 &a, float s) {
//   a.x += (int)s;
//   a.y += (int)s;
// }
// inline __host__ __device__ void operator*=(int2 &a, float s) {
//   a.x *= (int)s;
//   a.y *= (int)s;
// }
// inline __host__ __device__ int2 operator*(int2 a, float s) {
//   return make_int2(a.x * (int)s, a.y * (int)s);
// }
// inline __host__ __device__ void operator*=(int2 &a, int s) {
//   a.x *= (int)s;
//   a.y *= (int)s;
// }
// inline __host__ __device__ int2 operator*(int2 a, int s) {
//   return make_int2(a.x * (int)s, a.y * (int)s);
// }
// inline __host__ __device__ void operator+=(int2 &a, short s) {
//   a.x += (int)s;
//   a.y += (int)s;
// }
// inline __host__ __device__ void operator*=(int2 &a, short s) {
//   a.x *= (int)s;
//   a.y *= (int)s;
// }
// inline __host__ __device__ int2 operator*(int2 a, short s) {
//   return make_int2(a.x * (int)s, a.y * (int)s);
// }

// inline __host__ __device__ void operator+=(short2 &a, float s) {
//   a.x += (short)s;
//   a.y += (short)s;
// }
// inline __host__ __device__ void operator*=(short2 &a, float s) {
//   a.x *= (short)s;
//   a.y *= (short)s;
// }
// inline __host__ __device__ short2 operator*(short2 a, float s) {
//   return make_short2(a.x * (short)s, a.y * (short)s);
// }
// inline __host__ __device__ void operator+=(short2 &a, int s) {
//   a.x += (short)s;
//   a.y += (short)s;
// }
// inline __host__ __device__ void operator*=(short2 &a, int s) {
//   a.x *= (short)s;
//   a.y *= (short)s;
// }
// inline __host__ __device__ short2 operator*(short2 a, int s) {
//   return make_short2(a.x * (short)s, a.y * (short)s);
// }
// inline __host__ __device__ void operator+=(short2 &a, short s) {
//   a.x += (short)s;
//   a.y += (short)s;
// }
// inline __host__ __device__ void operator*=(short2 &a, short s) {
//   a.x *= (short)s;
//   a.y *= (short)s;
// }
// inline __host__ __device__ short2 operator*(short2 a, short s) {
//   return make_short2(a.x * (short)s, a.y * (short)s);
// }

// // Addition operator overload for vector types
// inline __host__ __device__ void operator+=(float2 &a, float2 s) {
//   a.x += s.x;
//   a.y += s.y;
// }
// inline __host__ __device__ void operator+=(float2 &a, short2 s) {
//   a.x += (float)s.x;
//   a.y += (float)s.y;
// }
// inline __host__ __device__ void operator+=(float2 &a, int2 s) {
//   a.x += (float)s.x;
//   a.y += (float)s.y;
// }
// inline __host__ __device__ void operator+=(int2 &a, int2 s) {
//   a.x += s.x;
//   a.y += s.y;
// }
// inline __host__ __device__ void operator+=(int2 &a, short2 s) {
//   a.x += (int)s.x;
//   a.y += (int)s.y;
// }
// inline __host__ __device__ void operator+=(short2 &a, short2 s) {
//   a.x += s.x;
//   a.y += s.y;
// }
// inline __host__ __device__ float2 operator+(float2 a, float2 s) {
//   return make_float2(a.x + s.x, a.y + s.y);
// }
// inline __host__ __device__ int2 operator+(int2 a, int2 s) {
//   return make_int2(a.x + s.x, a.y + s.y);
// }
// inline __host__ __device__ short2 operator+(short2 a, short2 s) {
//   return make_short2(a.x + s.x, a.y + s.y);
// }
namespace rtbf {

// Complex multiplication for vector types
template <typename T>
inline __host__ __device__ T complexMultiply(T a, T b) {
  T c;
  c.x = a.x * b.x - a.y * b.y;
  c.y = a.y * b.x + a.x * b.y;
  return c;
}

inline __host__ __device__ float sincf(float a) {
  return (a == 0.f) ? 1.f : sinpif(a) / (PI * a);
}

// Easy function to check the number of components in a datatype
__inline__ int checkVectorLength(short) { return 1; }
__inline__ int checkVectorLength(short2) { return 2; }
__inline__ int checkVectorLength(int) { return 1; }
__inline__ int checkVectorLength(int2) { return 2; }
__inline__ int checkVectorLength(float) { return 1; }
__inline__ int checkVectorLength(float2) { return 2; }
__inline__ int checkVectorLength(half) { return 1; }
__inline__ int checkVectorLength(half2) { return 2; }

// Load 1-data types from 1-data
__device__ __inline__ float loadFloat(float *dat, int idx) { return dat[idx]; }
__device__ __inline__ float loadFloat(half *dat, int idx) {
  return __half2float(dat[idx]);
}
__device__ __inline__ float loadFloat(short *dat, int idx) { return dat[idx]; }
__device__ __inline__ float loadFloat(int *dat, int idx) { return dat[idx]; }
// Load 2-data types from 1-data
__device__ __inline__ float2 loadFloat2(float *dat, int idx) {
  return make_float2(dat[idx], 0);
}
__device__ __inline__ float2 loadFloat2(half *dat, int idx) {
  return make_float2(__half2float(dat[idx]), 0);
}
__device__ __inline__ float2 loadFloat2(short *dat, int idx) {
  return make_float2(dat[idx], 0);
}
__device__ __inline__ float2 loadFloat2(int *dat, int idx) {
  return make_float2(dat[idx], 0);
}
// Load 2-data types from 2-data
__device__ __inline__ float2 loadFloat2(float2 *dat, int idx) {
  return dat[idx];
}
__device__ __inline__ float2 loadFloat2(half2 *dat, int idx) {
  return __half22float2(dat[idx]);
}
__device__ __inline__ float2 loadFloat2(short2 *dat, int idx) {
  return make_float2(dat[idx].x, dat[idx].y);
}
__device__ __inline__ float2 loadFloat2(int2 *dat, int idx) {
  return make_float2(dat[idx].x, dat[idx].y);
}
// Save 1-data type from 1-data
__device__ __inline__ void saveData(float *dat, int idx, float d) {
  dat[idx] = d;
}
__device__ __inline__ void saveData(half *dat, int idx, float d) {
  dat[idx] = __float2half(d);
}
__device__ __inline__ void saveData(short *dat, int idx, float d) {
  dat[idx] = d;
}
__device__ __inline__ void saveData(int *dat, int idx, float d) {
  dat[idx] = d;
}
// Save 2-data type from 1-data
__device__ __inline__ void saveData2(float2 *dat, int idx, float d) {
  dat[idx] = make_float2(d, 0.f);
}
__device__ __inline__ void saveData2(half2 *dat, int idx, float d) {
  dat[idx] = __float22half2_rn(make_float2(d, 0.f));
}
__device__ __inline__ void saveData2(short2 *dat, int idx, float d) {
  dat[idx] = make_short2(short(d), 0);
}
__device__ __inline__ void saveData2(int2 *dat, int idx, float d) {
  dat[idx] = make_int2(int(d), 0);
}
// Save 2-data type from 2-data
__device__ __inline__ void saveData2(float2 *dat, int idx, float2 d) {
  dat[idx] = d;
}
__device__ __inline__ void saveData2(half2 *dat, int idx, float2 d) {
  dat[idx] = __float22half2_rn(d);
}
__device__ __inline__ void saveData2(short2 *dat, int idx, float2 d) {
  dat[idx] = make_short2(short(d.x), short(d.y));
}
__device__ __inline__ void saveData2(int2 *dat, int idx, float2 d) {
  dat[idx] = make_int2(int(d.x), int(d.y));
}

}  // namespace rtbf

#endif