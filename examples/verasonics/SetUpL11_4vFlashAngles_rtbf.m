% B-mode imaging with Verasonics using rtbf to perform plane wave beamforming.
clear all

%% Define acquisition parameters
P.configName = 'L11-4v_PlnWv_rtbf';
P.nxmits = 25;  % Number of transmits
P.totalAngle = 30;  % Total angular span
P.fc = 7.24e6;  % Center frequency
P.sampleMode = 'NS200BW';  % [NS200BW, BS100BW, BS50BW] supported
P.imageBufferSize = 10;  % Number of frames
P.rxStartMm = 0;  % Receive start depth
P.rxDepthMm = 40;  % Initial Receive end depth
P.rxMaxDepthMm = 50;  % Maximum end depth
P.focDepthMm = 0;  % 0 for plane waves, > 0 for focused tx, < 0 for diverging
P.acqFPS = 100;     % frames per second of data being saved
P.acqPRF = 6e3;  % pulse repetition frequency
P.TxVoltage = 0;  % To be populated later
P.extraBuffer = 1.8; % Factor to increase buffer length by; not sure why this is needed
P.txfn = 0;  % Transmit f-number = 0: Use all elements

%% Define reconstruction parameters
R.fovWidthMm = 25;  % Width of the field of view
R.outspwl = 3;  % Output pixel grid density (samples per wavelength)
R.useAsIQ = 1;  % 0: RF data (NS200BW only), 1: direct IQ sampling
R.vxfn = 1;  % Virtual source transmit f-number
R.rxfn = 1;  % Receive f-number

%% Populate the rest of P and R (do not modify!!!)
R.initialized = false;
% Set angles
if P.nxmits == 1
    P.angles = 0;    P.dtheta = 0;
else
    P.angles = linspace(-P.totalAngle/2,P.totalAngle/2,P.nxmits) * pi/180;
    P.dtheta = abs(diff(P.angles(1:2)));
end

%% Define system parameters.
Resource.Parameters.numTransmit = 128;      % number of transmit channels.
Resource.Parameters.numRcvChannels = 128;    % number of receive channels.
Resource.Parameters.speedOfSound = 1540;    % set speed of sound in m/sec before calling computeTrans
Resource.Parameters.verbose = 2;
Resource.Parameters.initializeOnly = 0;
Resource.Parameters.simulateMode = 1;
%  Resource.Parameters.simulateMode = 1 forces simulate mode, even if hardware is present.
%  Resource.Parameters.simulateMode = 2 stops sequence and processes RcvData continuously.

%% Specify Trans structure array.
Trans.name = 'L11-4v';
Trans.units = 'wavelengths'; % Explicit declaration avoids warning message when selected by default
Trans.frequency = P.fc * 1e-6;  % Frequency in MHz
Trans = computeTrans(Trans);  % L11-4v transducer is 'known' transducer so we can use computeTrans.
Trans.maxHighVoltage = 50;  % set maximum high voltage limit for pulser supply.

%% Define lengths in wavelengths now that we have Trans.frequency
mmToWvln = @(mm) mm*1e-3 * Trans.frequency*1e6 / Resource.Parameters.speedOfSound;
wvlnToMm = @(wvln) wvln*Resource.Parameters.speedOfSound / (Trans.frequency*1e6) * 1e3;
P.rxStart = mmToWvln(P.rxStartMm);
P.rxDepth = mmToWvln(P.rxDepthMm);
R.fovWidth = mmToWvln(R.fovWidthMm);
P.rxMaxDepth = mmToWvln(P.rxMaxDepthMm);
P.focDepth = mmToWvln(P.focDepthMm);

%% Specify PData structure array.
dx = 1 / R.outspwl;
dy = 0;
dz = 1 / R.outspwl;
imgx = 0:dx:R.fovWidth; imgx = imgx - mean(imgx);
imgy = 0;
imgz = P.rxStart:dz:P.rxDepth;
PData(1).PDelta = [dx, dy, dz];  % Pixel spacing
PData(1).Size = [length(imgz), length(imgx), length(imgy)];
PData(1).Origin = [imgx(1), imgy(1), imgz(1)]; % x,y,z of upper lft crnr.
% No PData.Region specified, so a default Region for the entire PData array will be created by computeRegions.

%% Specify Media object. 'pt1.m' script defines array of point targets.
pt1;
Media.attenuation = -0.5;
Media.function = 'movePoints';
Media.numPoints = size(Media.MP,1);

%% Specify Resources.
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = P.nxmits*4096*2; % this size allows for maximum range
Resource.RcvBuffer(1).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(1).numFrames = 30;    % 30 frames stored in RcvBuffer.
Resource.InterBuffer(1).numFrames = 1;   % one intermediate buffer needed.
Resource.ImageBuffer(1).numFrames = 10;
Resource.DisplayWindow(1).Title = P.configName;
Resource.DisplayWindow(1).pdelta = 0.35;
ScrnSize = get(0,'ScreenSize');
DwWidth = ceil(PData(1).Size(2)*PData(1).PDelta(1)/Resource.DisplayWindow(1).pdelta);
DwHeight = ceil(PData(1).Size(1)*PData(1).PDelta(3)/Resource.DisplayWindow(1).pdelta);
Resource.DisplayWindow(1).Position = [250,(ScrnSize(4)-(DwHeight+150))/2, ...  % lower left corner position
                                      DwWidth, DwHeight];
Resource.DisplayWindow(1).ReferencePt = [PData(1).Origin(1),0,PData(1).Origin(3)];   % 2D imaging is in the X,Z plane
Resource.DisplayWindow(1).Type = 'Verasonics';
Resource.DisplayWindow(1).numFrames = 20;
Resource.DisplayWindow(1).AxesUnits = 'mm';
Resource.DisplayWindow(1).Colormap = gray(256);

%% Specify Transmit waveform structure.
TW(1).type = 'parametric';
TW(1).Parameters = [Trans.frequency,.67,2,1];

%% Specify TX structure array.
TX = repmat(struct('waveform', 1, ...
                   'Origin', [0.0,0.0,0.0], ...
                   'Apod', ones(Resource.Parameters.numTransmit,1)', ...
                   'focus', P.focDepth, ...0.0, ...
                   'Steer', [0.0,0.0], ...
                   'Delay', zeros(1,Trans.numelements)), 1, P.nxmits);
% - Set event specific TX attributes.
for n = 1:P.nxmits
    TX(n).Steer(1) = P.angles(n);
    TX(n).Delay = computeTXDelays(TX(n));
end

%% Specify TPC structures.
TPC(1).P.name = 'Imaging';
TPC(1).maxHighVoltage = 30;

%% Specify TGC Waveform structure.
TGC.CntrlPts = [0,297,424,515,627,764,871,1000];
TGC.rangeMax = P.rxDepth;
TGC.Waveform = computeTGCWaveform(TGC);

%% Specify Receive structure arrays.
% - We need P.nxmits Receives for every frame.
maxAcqLength = ceil(sqrt(P.rxDepth^2 + ((Trans.numelements-1)*Trans.spacing)^2));
Receive = repmat(struct('Apod', ones(1,Trans.numelements), ...
                        'startDepth', P.rxStart, ...
                        'endDepth', maxAcqLength,...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'sampleMode', 'NS200BW', ...
                        'mode', 0, ...
                        'callMediaFunc', 0), 1, P.nxmits*Resource.RcvBuffer(1).numFrames);

% - Set event specific Receive attributes for each frame.
for i = 1:Resource.RcvBuffer(1).numFrames
    Receive(P.nxmits*(i-1)+1).callMediaFunc = 1;
    for j = 1:P.nxmits
        Receive(P.nxmits*(i-1)+j).framenum = i;
        Receive(P.nxmits*(i-1)+j).acqNum = j;
    end
end

%% Specify Recon structure arrays. -- Not needed for rtbf
% - We need one Recon structure which will be used for each frame.
Recon = struct('senscutoff', 0.6, ...
               'pdatanum', 1, ...
               'rcvBufFrame',-1, ...
               'IntBufDest', [1,1], ...
               'ImgBufDest', [1,-1], ...
               'RINums', 1:P.nxmits);

%% Define ReconInfo structures. -- Not needed for rtbf
% We need P.nxmits ReconInfo structures for P.nxmits steering angles.
ReconInfo = repmat(struct('mode', 'accumIQ', ...  % default is to accumulate IQ data.
                   'txnum', 1, ...
                   'rcvnum', 1, ...
                   'regionnum', 1), 1, P.nxmits);
% - Set specific ReconInfo attributes.
if P.nxmits > 1
    ReconInfo(1).mode = 'replaceIQ'; % replace IQ data
    for j = 1:P.nxmits  % For each row in the column
        ReconInfo(j).txnum = j;
        ReconInfo(j).rcvnum = j;
    end
    ReconInfo(P.nxmits).mode = 'accumIQ_replaceIntensity'; % accum and detect
else
    ReconInfo(1).mode = 'replaceIntensity';
end

%% Specify Process structure array.
import vsv.seq.function.ExFunctionDef;
EF(1).Function = ExFunctionDef('beamformRawData', @beamformRawData);
Process(1).classname = 'External';
Process(1).method = 'beamformRawData';
Process(1).Parameters = {'srcbuffer','receive', ... % buffer to process
                         'srcbufnum',1, ...         % live buffer
                         'srcframenum',-1, ...      % most recent frame
                         'dstbuffer','image', ...
                         'dstbufnum', 1, ...     
                         'dstframenum', -1};     
                         % Specify Process structure array.
pers = 20;
Process(2).classname = 'Image';
Process(2).method = 'imageDisplay';
Process(2).Parameters = {'imgbufnum',1,...   % number of buffer to process.
                         'framenum',-1,...   % (-1 => lastFrame)
                         'pdatanum',1,...    % number of PData structure to use
                         'pgain',1.0,...            % pgain is image processing gain
                         'reject',2,...      % reject level
                         'persistMethod','simple',...
                         'persistLevel',pers,...
                         'interpMethod','4pt',...
                         'grainRemoval','none',...
                         'processMethod','none',...
                         'averageMethod','none',...
                         'compressMethod','power',...
                         'compressFactor',40,...
                         'mappingMethod','full',...
                         'display',1,...      % display image after processing
                         'displayWindow',1};  

%% Specify SeqControl structure arrays.
% Compute the time between the last acquisition and the next frame in usec
pulseTTNA = 1e6 / P.acqPRF;
frameTTNA = 1e6 / P.acqFPS - (pulseTTNA * (P.nxmits - 1));
SeqControl = [];

% syntax: addSeqControl(command, *argument, *condition, *label) [*optional]
addSeqControl('jump', 1, [], 'vsx_recon');
addSeqControl('jump', 1, [], 'rtbf_recon');
addSeqControl('timeToNextAcq', pulseTTNA);  % Pulse time to next acq
addSeqControl('timeToNextAcq', frameTTNA);  % Frame time to next acq
addSeqControl('returnToMatlab');  % Return to MATLAB

% Specify Event structure arrays.
% syntax: addEvent(info, tx, rcv, recon, process, seqControl)
Event = [];
% The first set of Events are for VSX reconstruction and processing
vsx_recon_startEvent = length(Event)+1;
SeqControl(1).argument = vsx_recon_startEvent;
for i = 1:Resource.RcvBuffer(1).numFrames  % Loop through frames
    for j = 1:P.nxmits  % Loop through angles
        addEvent(sprintf('Plane wave %d', j), j, P.nxmits*(i-1)+j, 0, 0, 3);
    end
    % transfer frame to host buffer
    addSeqControl('transferToHost');
    % modify last acquisition Event's seqControl
    Event(end).seqControl = [4,length(SeqControl)];
    addEvent('VSX recon and process', 0, 0, 1, 2, 5);
end
addEvent('Jump back', 0, 0, 0, 0, 1);

% An identical set of Events are used for rtbf reconstruction and processing
rtbf_recon_startEvent = length(Event)+1;
SeqControl(2).argument = rtbf_recon_startEvent;
for i = 1:Resource.RcvBuffer(1).numFrames  % Loop through frames
    for j = 1:P.nxmits  % Loop through angles
        addEvent(sprintf('Plane wave %d', j), j, P.nxmits*(i-1)+j, 0, 0, 3);
    end
    % transfer frame to host buffer
    addSeqControl('transferToHost');
    % modify last acquisition Event's seqControl
    Event(end).seqControl = [4,length(SeqControl)];
    addEvent('GPU recon', 0, 0, 0, 1, 0);
    addEvent('process', 0, 0, 0, 2, 5);
end
addEvent('Jump back', 0, 0, 0, 0, 2);

%% User specified UI Control Elements
UI = [];
% Add toggle button to turn on RTBF
addUIToggleButton('UserB5', 'Use RTBF', @RTBFToggleCallback);
% Add sensitivity cutoff
addUISlider('UserB7', 'Sens. Cutoff', 0, 1, Recon(1).senscutoff, 0.025, 0.1, '%1.3f', @SensCutoffCallback);
% Add range-change cutoff
AxesUnit = 'wls';
rmin = 64;
rmax = P.rxMaxDepth;
rcur = P.rxDepth;
% If DisplayWindow is showing axes units, convert values
if isfield(Resource.DisplayWindow(1),'AxesUnits')&&~isempty(Resource.DisplayWindow(1).AxesUnits)
    if strcmp(Resource.DisplayWindow(1).AxesUnits,'mm');
        AxesUnit = 'mm'; rmin = wvlnToMm(rmin); rmax = wvlnToMm(rmax); rcur = wvlnToMm(rcur);
    end
end
addUISlider('UserA1', 'Range', rmin, rmax, rcur, 0.1, 0.2, '%3.0f', @RangeChangeCallback);

% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 1;

% Save all the structures to a .mat file.
filename = 'MatFiles/L11-4vFlashAngles';
save(filename)
VSX;
return

% Utility to quickly add a SeqControl entry
function addSeqControl(cmd, arg, con, lab)
    if nargin < 2, arg = []; end
    if nargin < 3, con = []; end
    if nargin < 4, lab = []; end
    % Load SeqControl from caller workspace, or initialize empty
    try SeqControl = evalin('caller', 'SeqControl'); catch SeqControl = []; end
    % Define new SeqControl
    SC.command = cmd;
    SC.argument = arg;
    SC.condition = con;
    SC.label = lab;
    % Assign SeqControl
    assignin('caller', 'SeqControl', [SeqControl, SC]);
end

% Utility to quickly add an Event entry
function addEvent(info, tx, rcv, recon, process, seqControl)
    % Load Event from caller workspace, or initialize empty
    try Event = evalin('caller', 'Event'); catch Event = []; end
    % Define new Event
    E.info = info;
    E.tx = tx;
    E.rcv = rcv;
    E.recon = recon;
    E.process = process;
    E.seqControl = seqControl;
    % Assign Event
    assignin('caller', 'Event', [Event, E]);
end

% Utility to quickly add a UI slider control
function addUISlider(loc, label, vmin, vmax, vinit, smallstep, largestep, format, callback)
    % Load UI from caller workspace, or initialize empty
    try UI = evalin('caller', 'UI'); catch UI = []; end
    ui.Control = {loc, ...
        'Style', 'VsSlider', ...
        'Label', label, ...
        'SliderMinMaxVal', [vmin, vmax, vinit], ...
        'SliderStep', [smallstep, largestep], ...
        'ValueFormat', format};
    ui.Callback = callback;
    assignin('caller', 'UI', [UI, ui]);
end

% Utility to quickly add a UI PushButton control
function addUIPushButton(loc, label, callback)
    % Load UI from caller workspace, or initialize empty
    try UI = evalin('caller', 'UI'); catch UI = []; end
    ui.Control = {loc, 'Style', 'VsPushButton', 'Label', label};
    ui.Callback = callback;
    assignin('caller', 'UI', [UI, ui]);
end

% Utility to quickly add a UI ToggleButton control
function addUIToggleButton(loc, label, callback)
    % Load UI from caller workspace, or initialize empty
    try UI = evalin('caller', 'UI'); catch UI = []; end
    ui.Control = {loc, 'Style', 'VsToggleButton', 'Label', label};
    ui.Callback = callback;
    assignin('caller', 'UI', [UI, ui]);
end

%% Callback routines
function RTBFToggleCallback(~, ~, UIValue)
    if UIValue, startEvent = evalin('base', 'rtbf_recon_startEvent');
    else, startEvent = evalin('base', 'vsx_recon_startEvent');
    end
	Control = evalin('base', 'Control');
	Control(1).Command = 'set&Run';
	Control(1).Parameters = {'Parameters', 1, 'startEvent', startEvent};
	assignin('base', 'Control', Control);
	% evalin('base', sprintf('Resource.Parameters.startEvent = %d;', startEvent));
end

% Sensitivity cutoff change
function SensCutoffCallback(~, ~, UIValue)
ReconL = evalin('base', 'Recon');
for i = 1:size(ReconL,2)
    ReconL(i).senscutoff = UIValue;
end
assignin('base','Recon',ReconL);
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'Recon'};
assignin('base','Control', Control);
return
end

% Range change
function RangeChangeCallback(~, ~, UIValue)
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','P.rxDepth'));
    return
end

P = evalin('base','P');
Trans = evalin('base','Trans');
Resource = evalin('base','Resource');
PData = evalin('base','PData');
Receive = evalin('base', 'Receive');
TGC = evalin('base', 'TGC');

% Update P's rxDepth value
P.rxDepth = UIValue;
if isfield(Resource.DisplayWindow(1),'AxesUnits')&&~isempty(Resource.DisplayWindow(1).AxesUnits)
    if strcmp(Resource.DisplayWindow(1).AxesUnits,'mm');
        scaleToWvl = Trans.frequency/(Resource.Parameters.speedOfSound/1000);
        P.rxDepth = UIValue*scaleToWvl;
    end
end
assignin('base','P',P);

% Update PData and Resource DisplayWindow
PData(1).Size(1) = ceil((P.rxDepth-P.rxStart)/PData(1).PDelta(3));
PData(1).Region = computeRegions(PData(1));
Resource.DisplayWindow(1).Position(4) = ceil(PData(1).Size(1)*PData(1).PDelta(3)/Resource.DisplayWindow(1).pdelta);
assignin('base','PData',PData);
assignin('base','Resource',Resource);

% Update Receive
maxAcqLength = ceil(sqrt(P.rxDepth^2 + ((Trans.numelements-1)*Trans.spacing)^2));
for i = 1:size(Receive,2)
    Receive(i).rxDepth = maxAcqLength;
end
assignin('base','Receive',Receive);

% Update TGC
TGC.rangeMax = P.rxDepth;
TGC.Waveform = computeTGCWaveform(TGC);
assignin('base','TGC',TGC);

% Update Control and execute changes
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'PData','InterBuffer','ImageBuffer','DisplayWindow','Receive','TGC','Recon'};
assignin('base','Control', Control);
assignin('base', 'action', 'displayChange');

% Update rtbf
clear rtbf_mex
evalin('base','R.initialized = 0;');
end

function imgbuf = beamformRawData(rcvbuf)
    if ~evalin('base','R.initialized')       
        % Get user parameters from base workspace
        % Grab relevant data structures from base workspace
        P = evalin('base', 'P');
        R = evalin('base', 'R');
        Resource = evalin('base','Resource');
        Trans = evalin('base','Trans');
        TX = evalin('base','TX');
        TW = evalin('base','TW');
        Receive = evalin('base','Receive');
        PData = evalin('base','PData');

        %% Parse structures
        nsamps = Receive(1).endSample;  % Number of samples per acquisition
        nxmits = P.nxmits;  % Number of transmits
        nchans = Resource.Parameters.numRcvChannels;  % Number of acquisition channels
        nelems = size(Trans.ElementPos, 1);  % Number of total transducer elements
        fc = TW(1).Parameters(1) * 1e6;  % Hz
        fs = fc * Receive(1).samplesPerWave;  % Hz
        c0 = Resource.Parameters.speedOfSound;  % m/s
        wl = c0 / fc;  % Wavelength, m/cycle
        elpos = single(Trans.ElementPos(:,1:3))' * wl;  % m/s
        sampleMode = Receive(1).sampleMode;  % e.g., 'NS200BW'

        %% Get transmit information
        txori = zeros(3,nxmits,'single');  % (x,y,z) coordinates, m
        txdir = zeros(2,nxmits,'single');  % (theta, phi) angles, rad
        txfoc = zeros(3,nxmits,'single');  % (x,y,z) coordinates, m
        time0 = zeros(nxmits, 1, 'single');  % Time zero offset, s
        for i = 1:nxmits
            txori(:,i) = TX(i).Origin * wl;
            txdir(:,i) = TX(i).Steer;
            v = [...
                sin(TX(i).Steer(1)) * cos(TX(i).Steer(2)), ...
                sin(TX(i).Steer(1)) * sin(TX(i).Steer(2)), ...
                cos(TX(i).Steer(1))];
            txfoc(:,i) = (TX(i).focus * v + TX(i).Origin) * wl;
            % The time zero definition is critical yet complicated to define.
            % First, we find the time correction due to the transmit profile:
            %   Focused TX:     t0 = the maximum TX delay + time to focal point
            %   Diverging TX:   t0 = the minimum TX delay + time to focal point
            %   Plane Wave TX:  t0 = when the wave passes through the origin
            if     TX(i).focus >  0, t0 = max(TX(i).Delay) / fc + txfoc(3,i) / c0;
            elseif TX(i).focus <  0, t0 = min(TX(i).Delay) / fc + txfoc(3,i) / c0;
            elseif TX(i).focus == 0, t0 = interp1(elpos(1,:), TX(i).Delay, mean(elpos(1,:))) / fc;
            end
            % We must also account for the pulse envelope shape and the lens correction.
            time0(i,:) = t0 + (TW(1).peak + 2*Trans.lensCorrection) / fc;
        end

        %% Reconstruction information
        xpos = single(PData.Origin(1) + (1:PData.Size(2)) * PData.PDelta(1)) * wl;
        xpos = xpos(1:end-1); xpos = xpos - mean(xpos);
        zpos = single(PData.Origin(3) + (1:PData.Size(1)) * PData.PDelta(3)) * wl;
        [xx, zz] = meshgrid(xpos, zpos);  % Find coordinates of every pixel in grid
        pxpos = cat(3, xx, xx*0, zz);
        pxpos = permute(pxpos, [3 1 2]);  % Make [(xyz), rows, columns]

        %% Define rtbf structs
        % VSXFormatter
        V.operator = 'VSXFormatter';
        V.nsamps = nsamps;
        V.nxmits = nxmits;
        V.nelems = nchans;
        V.nchans = nchans;
        if R.useAsIQ
            V.sampleMode = sampleMode;
        else
            V.sampleMode = 'HILBERT';
        end
        % Focus
        F.operator = 'Focus';
        F.pxpos = pxpos;
        F.elpos = elpos;
        F.time0 = time0;
        if P.focDepth == 0, F.txdat = txdir;  % If plane wave, supply tx direction
        else, F.txdat = txfoc;  % Otherwise, supply tx focus
        end
        F.fs = fs;  % Sampling frequency
        if R.useAsIQ
            F.fc = 0;  % Center frequency (VSX does baseband sampling)
            F.fdemod = fc;  % Demodulation frequency (VSX does baseband sampling)
        else
            F.fc = fc;  % Center frequency
        end
        F.c0 = c0;  % Sound speed
        F.txbf_mode = 'SUM';  % Let Focus sum the plane wave angles
        F.rxbf_mode = 'IDENTITY';  % Do not let Focus sum the receive elements
        F.txfnum = R.vxfn;  % Transmit f#
        F.rxfnum = R.rxfn;  % Receive f#
        % ChannelSum (last dimension by default)
        C.operator = 'ChannelSum';
        % Bmode (no compression)
        B.operator = 'Bmode'; B.compression = 'none';
        bimg = rtbf_mex(rcvbuf, V, F, C, B);

        % Mark as initialized
        evalin('base','R.initialized = 1;');
    else
        bimg = rtbf_mex(rcvbuf);
    end
    imgbuf = double(bimg * 2);  % Not sure why *2 is needed to match amplitudes
end